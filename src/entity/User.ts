import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nickname: string;

    @Column()
    dateOfBirth: number; 

    @Column()
    email: string;

    @Column({default: false})
    activated: boolean;

    @Column({default: false})
    enabled: boolean;

}
