import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./entity/User";
import { Server } from './lib/server';
import { UsersController, UsersRouting } from "./controllers/users";

createConnection().then(async connection => {

    const server = new Server();
    const repo = await connection.getRepository(User);
    const usersCtrl = new UsersController(repo);    
    const usersRouting = new UsersRouting(usersCtrl);
        
    server.addRoute(usersRouting.getRoutes());
    server.init();

}).catch(error => console.log(error));
