import * as express from "express";
import { UsersController } from "./users.controller";
import { Routing, IRouting } from "../../lib/routing";

export class UsersRouting extends Routing implements IRouting {
    constructor(private ctrl: UsersController) { 
        super();
    }

    getRoutes() {
        let router = express.Router();

        router.route('/users')
            .get(async (req, res) => this.send(res)(await this.ctrl.getAll()))
            .post(async (req, res) => this.send(res)(await this.ctrl.create(req.body)))

        router.route('/users/:id')
            .get(async (req, res) => this.send(res)(await this.ctrl.getOne(req.params.id)))
            .put(async (req, res) => this.send(res)(await this.ctrl.update(req.params.id, req.body)))
            .delete(async (req, res) => this.send(res)(await this.ctrl.remove(req.params.id)));

        return router;
    }
}