import { User } from "../../entity/User";
import { Controller, ErrRes } from "../../lib/controller";
import { UpdateResult, SaveOptions, FindManyOptions } from "typeorm";

export class UsersController extends Controller<User> {
    async remove(id: number, opts?: SaveOptions): ErrRes<UpdateResult> {
        return await this.update(id, { enabled: false }, opts);
    }

    private async checkIfExists(email: string) {
        const opts: FindManyOptions<User> = {
            where: {
                email
            }
        }
        let [err, [users, number]] = await this.errorIfy(this.repo.findAndCount(opts));
        if (err) {
            return Promise.reject(err);
        }
        return number > 0;
    }

    async createUser(u: User) {
        const exists = await this.checkIfExists(u.email)
        if (exists) {
            return false;
        }
        return await this.create(u);
    }
}