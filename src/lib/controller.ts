import { Repository, InsertResult, UpdateResult, DeepPartial, FindOneOptions, SaveOptions, RemoveOptions, FindManyOptions } from "typeorm";

export type ErrRes<T> = Promise<[boolean, T]>;

export abstract class Controller<T> {
    constructor(protected repo: Repository<T>) { }
    
    protected async errorIfy<T>(f: Promise<T>, msg?: string): ErrRes<T> {
        let error = false;
        let result = null;
        try {
            result = await f;
        } catch (e) {
            error = msg || e;
        }
        return [error, result];
    }

    async getAll(opts?: FindManyOptions): ErrRes<T[]> {
        return await this.errorIfy<T[]>(this.repo.find(opts));
    }
    async getOne(id: number, opts?: FindOneOptions): ErrRes<T> {
        return await this.errorIfy<T>(this.repo.findOne(id, opts));
    }
    async create(data: T, opts?: SaveOptions): ErrRes<InsertResult> {
        return await this.errorIfy<InsertResult>(this.repo.insert(data, opts));
    }
    async update(id: number, data: DeepPartial<T>, opts?: SaveOptions): ErrRes<UpdateResult> {
        return await this.errorIfy<UpdateResult>(this.repo.update(id, data, opts));
    }
    async remove(id: number, opts?: RemoveOptions): ErrRes<T | UpdateResult> {
        let [err, entity] = await this.getOne(id);
        if (err) {
            return Promise.reject(err);
        }
        return await this.errorIfy(this.repo.remove(entity, opts));
    }
    async destroy(entity: T, opts?: RemoveOptions): ErrRes<T> {
        return await this.errorIfy<T>(this.repo.remove(entity, opts));
    }
}