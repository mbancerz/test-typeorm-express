import { Response, Router } from 'express';

export interface IRouting {
    getRoutes(): Router;
}

export abstract class Routing {
    protected send = (res: Response) => (f: [boolean, any]) => {
        let [err, result] = f;
        if (err) {
            res.status(422).json(err);
        } else {
            res.status(200).json(result);
        }
    }
}
