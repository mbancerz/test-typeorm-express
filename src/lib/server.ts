import * as Express from 'express';

export class Server {
    private created: number;
    private app: Express.Application;
    private router: Express.Router;

    private getHealth = async (req, res) => {
        await res.status(200).send({ status: 'OK', created: this.created });
    }

    constructor() {
        this.app = Express();
        this.app.use(Express.json({strict: false}));
        this.router = Express.Router();
        this.app.get('/health', this.getHealth);
    }

    public addRoute(handler: Express.RequestHandler) {
        this.app.use(handler);
    }

    public init() {        
        this.app.listen(7878, () => {
            this.created = Date.now();
            console.log('Server stated @ 7878');
            console.log('---------------------------------------------')
        });
    }

}